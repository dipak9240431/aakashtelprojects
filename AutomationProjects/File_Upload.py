from selenium import webdriver
driver=webdriver.Chrome()
driver.implicitly_wait(10)
File_upload=driver.get("https://the-internet.herokuapp.com/upload");
filepath=File_upload.driver.find_element(By.ID,"file-upload").send_keys("selenium-snapshot.jpg")
submit=driver.find_element(By.ID,"file-submit").submit()
if(driver.page_source.find("File Uploaded!")):
    print("file upload success")
else:
    print("file upload not successful")
driver.quit()
import  pynput