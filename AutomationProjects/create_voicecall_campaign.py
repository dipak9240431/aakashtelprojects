from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.action_chains import ActionChains
import time
import random
import string

driver = webdriver.Chrome()
wait = WebDriverWait(driver, 10)
driver.get('https://apps.aakashtel.com/login')
driver.maximize_window()
Login_Email = driver.find_element(By.ID, ':r0:')
Login_Email.send_keys('dipak@esignature.com.np')
Login_Password = driver.find_element(By.ID, ':r1:')
Login_Password.send_keys('Admin@123#')
Login_Button = driver.find_element(By.XPATH, '//*[@id="__next"]/div[1]/div/div/div[2]/div/div/div[3]/button')
Login_Button.click()
time.sleep(5)
create_Broadcast_Camapaign = driver.find_element(By.XPATH, '//*[@id="create-campaign"]')
create_Broadcast_Camapaign.click()
time.sleep(3)


# Generate a random name for the campaign
def generate_random_name(length):
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for _ in range(length))


# Input Field of campaign
campaing_input = driver.find_element(By.XPATH,
                                     '/html/body/div/div[1]/div[2]/div[3]/div[1]/form/div[1]/div/div/div[2]/div/div/input')
time.sleep(5)

# Keep track of used campaign names
used_names = set()
# Generate and check a campaign name
while True:
    campaign_name = generate_random_name(10)  # You can change the length if you want
    # validate if the campaign already created or not
    if campaign_name not in used_names:
        used_names.add(campaign_name)
        break
campaing_input.send_keys(campaign_name)
time.sleep(2)

# Creating Dummy Phone Numbers
# You comment this generating random number if you upload CSV File and vice-versa

dummy_phone_numbers = []  # Empty list of Dummy Numbers
for _ in range(0, 3):  # range of 20 phonenumbers
    dummy_phone_numbers.append("98" + ''.join(random.choice('012345000015456126789') for _ in
                                              range(8)))  # number starts with 98 append with generated random number

# creating Dummy Phone Numbers
Input_Element = driver.find_element(By.XPATH,
                                    '/html/body/div/div[1]/div[2]/div[3]/div[1]/form/div[2]/div[1]/div/div[2]/div/label/div/div/div/div[2]/div/div/input')
Input_Element.click()
time.sleep(2)
# Loop
for phone_number in dummy_phone_numbers:
    Input_Element.send_keys(phone_number)
    # Input_Element.send_keys('9825648134')                     #//*[@id="upload-mobile-number"]
    time.sleep(2)
    # thihs line of code will be hit enter without press enter button from the keyboard
    Input_Element.send_keys(Keys.RETURN)
time.sleep(3)
wait=WebDriverWait(driver,10)
Select_Group = driver.find_element(By.XPATH,
                                   '//*[@id="__next"]/div[1]/div[2]/div[3]/div[1]/form/div[2]/div[2]/button[2]')
Select_Group.click()
time.sleep(3)
# Select dropdown
# dropdown = driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/div[2]/div/div[1]/div[2]/div')
# # if you Need to change to other groups the just chage text here
# select=Select(dropdown)
# select.select_by_index(2)
# # Find all the options within the dropdown
# time.sleep(5)
# Locate and click to open the custom dropdown
dropdown_opener = driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/div[2]/div/div[1]/div[2]/div')
dropdown_opener.click()

# Wait for the dropdown options to become visible
wait = WebDriverWait(driver, 10)
dropdown_options = wait.until(EC.visibility_of_all_elements_located((By.XPATH, '/html/body/div[3]/div[3]/div/div[2]/div/div[1]/div[2]/div/*')))
+97715970387
# Iterate through the dropdown options and select the desired one


# Replace the XPath below with the correct one for your dropdown options
wait = WebDriverWait(driver, 10)
desired_option_text = 'Testing Group'
for option in dropdown_options:
    if option.text == desired_option_text:
        driver.execute_script("arguments[0].click();", option)
        break

time.sleep(5)
# #After select group click on ok button
Ok = driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/div[3]/button[1]')
Ok.click()
time.sleep(5)

# # Use WebDriverWait properly
# wait = WebDriverWait(driver, 30)  # Adjust the timeout as needed


# In this bulkFile Upload Section not given premission to go to specific location. Need to talk with developer.


# #Xpath of the Upload csv file
# upload_csv_file = wait.until(EC.element_to_be_clickable(
#     (By.XPATH, '//*[@id="__next"]/div[1]/div[2]/div[3]/div[1]/form/div[2]/div[2]/div/div/button')))
# upload_csv_file.click()

# Click the "Choose File" button
# Choose_FIle = wait.until(EC.element_to_be_clickable(
#     (By.XPATH, '/html/body/div[3]/div[3]/div[3]/button/div/p')))
# Choose_FIle.click()
#
# # Locate the file input element and send the file path
#   # path of file
# file_input = wait.until(EC.presence_of_element_located(
#     (By.ID, 'CloudUploadIcon')))
# #will upload the csv file
# File_Path = "/home/e/Downloads/hello1.txt"
# file_input.send_keys(File_Path)

time.sleep(5)
# Bulk File Upload button section

# Upload=driver.find_element(By.XPATH,'/html/body/div[3]/div[3]/div[5]/button')
# Upload.click()
# time.sleep(3)

#
# element = wait.until(EC.element_to_be_clickable((By.XPATH, '/html/body/div/div[1]/div[2]/div[3]/div[1]/form/div[3]/div/div/div[2]/div/div/div')))
# element.click()
time.sleep(5)

# Note: Since this is not a <select> element, you don't need to use Select here.


# Dropdown Meanu for DID numbers.if multiple DID numbers are present

# Will be selected the specific DID number from Dropdown meanu.in this case 87 will be selected.

DID_Dropdown=wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@id="demo-simple-select"]')))
DID_Dropdown.click()
desired_DID_Number='+97715970387'
for DID_Number in desired_DID_Number:
    if DID_Number==desired_DID_Number:
        DID_Number.click()
        break
time.sleep(5)

#Choose Voice
Choose_Voice=driver.find_element(By.XPATH,'//*[@id="__next"]/div[1]/div[2]/div[3]/div[1]/form/div[4]/div[1]/div/div[2]/div/div/div/div/div/button')
select=Select(Choose_Voice)
select.select_by_index(1)
time.sleep(5)
driver.quit()


