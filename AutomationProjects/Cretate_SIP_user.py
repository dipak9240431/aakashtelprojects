from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions
from faker import Faker

fake = Faker()
import time

driver = webdriver.Chrome()
wait = WebDriverWait(driver, 10)
driver.get('https://apps.aakashtel.com/login')
driver.maximize_window()
Login_Email = driver.find_element(By.ID, ':r0:')
Login_Email.send_keys('dipak@esignature.com.np')
Login_Password = driver.find_element(By.ID, ':r1:')
Login_Password.send_keys('Admin@123#')
Login_Button = driver.find_element(By.XPATH, '//*[@id="__next"]/div[1]/div/div/div[2]/div/div/div[3]/button')
Login_Button.click()
time.sleep(5)

# click on staff Settings
wait = WebDriverWait(driver, 10)

Staff_Settings = driver.find_element(By.XPATH, '//*[@id="__next"]/div[1]/div[2]/div[2]/div/div[4]/button/span')
Staff_Settings.click()
time.sleep(3)

# Click on User
User = driver.find_element(By.XPATH, '//*[@id="__next"]/div[1]/div[2]/div[2]/div/div[5]/div[1]')
User.click()
time.sleep(2)
Add_New_user = driver.find_element(By.XPATH,
                                   '/html/body/div[1]/div[1]/div[2]/div[3]/div[1]/div[2]/div/div/div[2]/button')
Add_New_user.click()
time.sleep(2)

# user name using faker library
fake_name = fake.name()
Full_Name = driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/form/div[1]/div/div/input')
Full_Name.send_keys(fake_name)
time.sleep(5)

# To create 4 Digit user ID Using Faker library
four_digitID = fake.random.randint(1000, 9999)
User_ID = driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/form/div[2]/div/div/input')
User_ID.send_keys(four_digitID)
time.sleep(5)

# Generating the emails
Fake_Email = fake.email()
UserEmail = driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/form/div[3]/div/div/input')
UserEmail.send_keys(Fake_Email)
time.sleep(5)

# set user password
userPassword = driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/form/div[4]/div/div/div/input').send_keys(
    'Test@123#')
ConfirmPassword = driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/form/div[5]/div/div/div/input').send_keys(
    'Test@123#')
time.sleep(3)

# User Phone Number
fake_mobile_number = "98" + str(fake.random_int(min=10000000, max=99999999))
MobileNumber = driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/form/div[6]/div/div/input')
MobileNumber.send_keys(fake_mobile_number)

# Address of a user
Fake_Address = fake.address()  # will generte one word address
Address = driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/form/div[7]/div/div/input')
Address.send_keys(Fake_Address)
time.sleep(4)

# Select DID Number form the dropdown meanu

DID_Dropdown = driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/form/div[8]/div/div/div/div[1]/div[2]/input')
time.sleep(5)
# provide particular did number
search_DID = "+97715970386"
DID_Dropdown.send_keys(search_DID)
DID_Dropdown.send_keys(Keys.ENTER)
time.sleep(5)

# Find the dropdown-like div element by XPath
Department_Dropdown = driver.find_element(By.XPATH,
                                          '/html/body/div[3]/div[3]/div/form/div[10]/div/div/div[1]/div[1]/div[2]/input')
time.sleep(5)

# Select Department form the dropdown meanu
search_text = "QA Dept"  # I can make a list of all the elements inside the dropdon and choose them randomly using rand()
Department_Dropdown.send_keys(search_text)
Department_Dropdown.send_keys(Keys.ENTER)
time.sleep(4)

# submit button for the Form
Submit_Btn = driver.find_element(By.XPATH, '/html/body/div[3]/div[3]/div/form/div[11]/button[1]')
Submit_Btn.click()
time.sleep(3)

driver.quit()
