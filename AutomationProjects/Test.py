import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Initialize the WebDriver (assuming Chrome WebDriver is in your PATH)
driver = webdriver.Chrome()

# Navigate to the webpage
driver.get('https://www.globalsqa.com/demo-site/select-dropdown-menu/')

# Wait for the dropdown element to be clickable
wait = WebDriverWait(driver, 10)
dropdownMenu= driver.find_element(By.XPATH,'/html/body/div/div[1]/div[2]/div/div/div[2]/div/div/div/p/select')
Element=Select(dropdownMenu)
Element.select_by_visible_text('Nepal')
# Use the Select class to work with the dropdown
# Select the option by its visible text

time.sleep(10)

# Close the browser
driver.quit()
